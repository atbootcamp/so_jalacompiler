FROM sontiveros/so_base_img:1.0
WORKDIR /app
COPY . .
RUN apt-get update && apt-get install -y psmisc \
    && python -m pip install -r requirements.txt